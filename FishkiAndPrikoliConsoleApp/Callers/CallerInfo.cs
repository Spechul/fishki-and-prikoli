﻿using System;
using System.Runtime.CompilerServices;

namespace FishkiAndPrikoliConsoleApp.Callers
{
    // will work if no parameter passed in
    public class CallerInfo
    {
        public void CallerName ([CallerMemberName] string name="")
        {
            Console.WriteLine(name);
        }

        public void CallerFile([CallerFilePath] string name = "")
        {
            Console.WriteLine(name);
        }

        public void CallerLineNumber([CallerLineNumber] int line=0)
        {
            Console.WriteLine(line);
        }

    }
}