﻿using System;

namespace FishkiAndPrikoliConsoleApp.Partials
{
    public partial class PartialClass
    {
        public void Sooqa()
        {
            Console.WriteLine(nameof(Sooqa));
        }

        partial void ComePrintSome()
        {
            Console.WriteLine("ye, i'm ");
        }
    }
}