﻿using System;

namespace FishkiAndPrikoliConsoleApp.Partials
{
    public partial class PartialClass
    {
        public void Qoorwa()
        {
            Console.WriteLine(nameof(Sooqa));
        }

        partial void ComePrintSome();
    }
}