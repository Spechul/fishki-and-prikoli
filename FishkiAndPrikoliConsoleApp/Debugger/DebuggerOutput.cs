﻿using System.Collections.Generic;
using System.Diagnostics;

namespace FishkiAndPrikoliConsoleApp.Debugger
{
    //uncomment to override tostring for debugger
    //[DebuggerDisplay("name is {Name} and human is {Age} years old")]
    public class DebuggerOutput
    {
        public string Name { get; set; }

        //hover inside debug
        [DebuggerDisplay("{Age} years old")]

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public int Age { get; set; }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public List<string> Colors { get; set; } = new List<string>() {"Red", "Green", "Blue"};

        // hover inside debug
        public override string ToString()
        {
            return "hello there, 0b3y1can0b3y";
        }
    }
}