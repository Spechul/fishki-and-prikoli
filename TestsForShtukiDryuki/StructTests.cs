using System.Linq;
using NUnit.Framework;
using PerformanceDryuki.StructForce;

namespace TestsForShtukiDryuki
{
    public class Tests
    {
        private int[] arr = new int[10000];
        private int ans;

        [OneTimeSetUp]
        public void Setup()
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = i;
            }

            ans = arr.Sum();
        }

        [Test]
        public void Sum()
        {
            Assert.AreEqual(1, 1);
        }

        [Test]
        public void Sum_ShouldWork()
        {
            var sum = arr.Sum();
            Assert.AreEqual(sum, ans);
        }

        [Test]
        public void SumViaStruct_ShouldWork()
        {
            var sum = arr.Sum(new IntSummator());
            Assert.AreEqual(sum, ans);
        }
    }
}