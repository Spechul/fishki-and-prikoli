﻿namespace PerformanceDryuki.StructForce
{
    public static class ArrayExtensions
    {
        public static T Sum<T, TSummator>(this T[] array, TSummator summator)
            where TSummator : struct, ISummator<T>
        {
            var result = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                result = summator.Add(result, array[i]);
            }

            return result;
        }
    }
}