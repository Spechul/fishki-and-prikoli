﻿namespace PerformanceDryuki.StructForce
{
    public interface ISummator<T>
    {
        T Add(T left, T right);
    }
}